# Weather Prediction App

## Setup

1. Clone the repository:

    ```
    git clone https://gitlab.com/swapnagitlab/python-flask-weather-prediction-app.git
    ```

2. Install dependencies:

    ```
    pip install -r requirements.txt
    ```

3. Set up environment variables:

    Create a `.env` file in the project root with the following content:

    ```env
    OPENWEATHER_API_KEY=your_openweather_api_key
    ```

4. Run the application:


    python3 app.py
    ```

5. Access the app in your web browser at http://localhost:5000.

## Usage

1. Navigate to the main page (http://localhost:5000).
2. Enter latitude, longitude, and city name.
3. Click the "Fetch Weather" button.
4. View the real-time weather predictions.


## steps for pushing code again on gitlab
git add .
git commit -m "some comment"
git remote CI_PROJECT_PATH
git -m origin main
git push -f origin main
in case of getting error on push go to the gitlab settings -> repsoitory -> Protected Branches -> enable branch to push code on gitlab
git login gitlab username and put Access token created on gitlab settings as a password


## Important
please refer component_README.md to create a CI/CD catalog project with components which is located inside templates folder where my configuration.yml files located.

## License

This project is licensed under the MIT License.

## Problem Statement

Our project aims to address a crucial challenge faced by GitLab users in efficiently deploying Flask applications within their CI/CD pipelines. Many users encounter complexities related to dependency management, testing, and deployment, hindering the seamless integration of Flask applications into GitLab workflows.

## Articulation of the Problem

This project seeks to streamline the deployment process of Flask applications on GitLab. The challenges include managing dependencies, setting up testing environments, and ensuring a smooth Dockerized deployment. By providing a comprehensive solution, we aim to enhance the overall experience for GitLab users, making Flask application deployment a straightforward and efficient process.

## Innovativeness

Our solution introduces several innovative features:

- **Modular CI/CD pipeline:** Components like test, dockerize, and deploy are separated, promoting better organization and reusability.
- **Dockerized deployment:** Leveraging Docker for containerization ensures consistency across different environments, enhancing portability and reproducibility.
- **Release automation:** The inclusion of a release stage automates the creation of releases based on Git tags, simplifying version management.

## Overall Quality

The project exhibits excellence in:

- **Design:** The modular structure of the CI/CD pipeline enhances readability and maintainability.
- **User Experience:** Though CI/CD configurations are backend-focused, they are designed to be clear and understandable for contributors.
- **Technical Implementation:** Best practices, such as using Docker for containerization, are adhered to, ensuring a robust and standardized deployment process.

## Scalability

Our solution is designed to scale with:

- **Growing Customer Base:** The modular CI/CD pipeline can handle increasing project complexity and larger codebases.
- **Revenue:** The streamlined deployment process contributes to faster delivery, potentially increasing revenue through quicker time-to-market.
- **Operations:** Dockerized deployment ensures consistency, facilitating smooth operations across different environments.

## Total Addressable Market (TAM)

Capturing the market segment of GitLab users deploying Flask applications presents a substantial opportunity. This solution can significantly impact the efficiency and reliability of CI/CD processes for a broad audience, ranging from small development teams to large enterprises.

## Feasibility

Our solution is practical considering:

- **Available Resources:** The chosen technology stack, including Python, Flask, and Docker, is widely adopted and well-supported.
- **Technology:** Leveraging GitLab CI/CD features aligns with industry standards, ensuring compatibility and integration.
- **Time:** The modular structure allows for incremental development and testing, contributing to efficient use of time resources.
- **Barriers:** Any potential barriers, such as dependencies or compatibility issues, have been addressed, making the solution feasible for implementation.


