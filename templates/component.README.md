Project Overview

Welcome to the Weather Prediction Web App repository! This project empowers weather prediction using Python and Flask. The application is containerized with Docker for seamless deployment, and its CI/CD configuration is intelligently organized into modular components for enhanced clarity and efficiency.
Getting Started
Prerequisites

Make sure your project includes the following components:

    Dockerfile
    test_app.py
    gunicorn_start.sh

Project Setup:

    Create a new project on GitLab.
    Add a README.md file with a clear introduction to the component.
    Optionally, add a project avatar.
    Include a LICENSE.md file with your chosen license (e.g., MIT or Apache 2.0).

Example of directory structure component project:

plaintext

├── templates/
│   └── my-component.yml
├── Dockerfile
├── LICENSE.md
├── README.md
└── .gitlab-ci.yml

Component Configuration:

    Add a YAML configuration file for each component in the templates/ directory.
    Configure the .gitlab-ci.yml file to test components and release new versions.

Example component YAML (my-component.yml):

yaml

spec:
  inputs:
    stage:
      default: test
---
component-job:
  script: echo job 1
  stage: $[[ inputs.stage ]]
modify as per your project requirement




Catalog Project Configuration:

    Set the project as a catalog project:
        Navigate to project settings.
        Expand Visibility, project features, permissions.
        Turn on the "CI/CD Catalog project" toggle.

Publish a Release:

    Ensure the project has a description and a README.md file.
    Add at least one component in the templates/ directory.
    Create a new tag for the release, triggering the tag pipeline.
    The tag pipeline should include a job to create a release.

Example release job:

yaml

create-release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script: echo "Creating release $CI_COMMIT_TAG"
  rules:
    - if: $CI_COMMIT_TAG
  release:
    tag_name: $CI_COMMIT_TAG
    description: "Release $CI_COMMIT_TAG of components in $CI_PROJECT_PATH"

Best Practices:

    Write a clear README.md for each component.
    Test components in CI/CD pipelines.
    Avoid using global keywords and prefer inputs for dynamic configuration.
    Use semantic versioning for releases (e.g., 1.0.0, 2.1.3).
    Manage dependencies carefully, keeping them to a minimum.
    Consider creating a group to host multiple related components.

    The provided steps and structure are a starting point for setting up a CI/CD catalog project with components on GitLab. However, the specific requirements and configurations might vary based on your project and its dependencies. It's important to customize the instructions according to your project's needs.

CI/CD Components - gitlab-ci.yml File

This project's CI/CD configuration is modularized for improved maintainability. Each component serves a specific purpose:

    Test Component:
        File: templates/test-component.yml
        Purpose: Sets up the test environment and executes tests using unittest.

    Dockerize Component:
        File: templates/dockerize-component.yml
        Purpose: Dockerizes the application and pushes the image to the GitLab Container Registry.

    Deploy Component:
        File: templates/deploy-component.yml
        Purpose: Deploys the application using a Gunicorn-based script (gunicorn_start.sh).

Release 1.0.0

A tag and release have been created with version 1.0.0.
Dockerize:
    Authenticate with the GitLab Container Registry using CI/CD environment variables:
docker login
    Pull the Docker image with the release version before building:
docker build -t <registry>/<project>:1.0.0 .
    Tag the built image with the specific version:
docker tag <registry>/<project>:latest <registry>/<project>:1.0.0
    Push the built and tagged Docker image to the GitLab Container Registry:
docker push <registry>/<project>:1.0.0

Testing
To run the tests, refer to the test_app.py file within the Web-Application-apps folder and execute the provided GitLab CI/CD job named 'test'.

Deployment
For deployment, execute the provided GitLab CI/CD job named 'deploy'. The deployment script is located in gunicorn_start.sh.

License
This project is licensed under the MIT License - see the LICENSE file for details.
Attaching Binaries with Generic Packages

Utilize generic packages to store artifacts from a release or tag pipeline. Follow these steps:
    Push the artifacts to the Generic package registry.
    Attach the package link to the release.

This process generates release assets, publishes them as a generic package, and creates a release.
