from flask import Flask, render_template, request
from ship_weather_script import get_weather
import os
from dotenv import load_dotenv

load_dotenv()

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/fetch_weather', methods=['POST']) 
def fetch_weather():
    try:
        latitude = float(request.form['latitude'])
        longitude = float(request.form['longitude'])
        city_name = request.form['city_name']

        api_key = os.getenv('OPENWEATHER_API_KEY')
        
        weather_data = get_weather(api_key, latitude, longitude, city_name)

        if weather_data:
            temperature = convert_temperature_unit(weather_data['main']['temp'])
            icon_url = f"http://openweathermap.org/img/w/{weather_data['weather'][0]['icon']}.png"

            return render_template('weather_result.html', temperature=temperature,
                                   condition=weather_data['weather'][0]['description'],
                                   wind_speed=weather_data['wind']['speed'],
                                   wind_direction=weather_data['wind']['deg'],
                                   icon_url=icon_url)
        else:
            return "Unable to retrieve weather data."
    except ValueError:
        return "Invalid latitude, longitude, or city name. Please enter valid values."

def convert_temperature_unit(temperature):
    temperature_fahrenheit = (temperature * 9/5) + 32
    return temperature_fahrenheit

if __name__ == '__main__':
    app.run(debug=True)
